const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method

let getProfileById = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getMdSirProfileById(), [id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

let getTaskListBySearching = async (searchField) => {

    if (searchField.month != 0) {
        return new Promise((resolve, reject) => {
            connectionIntracoLPG.query(quaries.getTaskListBySearching(searchField), (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        });
    } else {
        return new Promise((resolve, reject) => {
            connectionIntracoLPG.query(quaries.getTaskListBySearchingWithYear(searchField), (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        });
    }

}


let getAppointmentListBySearching = async (searchField) => {

    if (searchField.month != 0) {
        return new Promise((resolve, reject) => {
            connectionIntracoLPG.query(quaries.getAppointmentListBySearching(searchField), (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        });
    }
    else {
        return new Promise((resolve, reject) => {
            connectionIntracoLPG.query(quaries.getAppointmentListBySearchingWithYear(searchField), (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        });
    }

}


module.exports = {
    getProfileById,
    getTaskListBySearching,
    getAppointmentListBySearching
}