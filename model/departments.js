const connectionIntracoLPG = require('../connection/connection');
const quaries = require('../query/lpg');


// Promices Method
let getList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDepartmentList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getList
}