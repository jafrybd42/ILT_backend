const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method

// old one
/* let getAppointmentList = async ( salesPersonId) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAppointmentList(),[ salesPersonId], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
} */


let getAppointmentList = async (searchFieldObject = {}, extraWhere = "") => {
    //console.log(extraWhere);
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAppointmentList(searchFieldObject, extraWhere), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}




let getDeactiveAppointmentList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDeactiveAppointmentList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let addNewAppointment = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addAppointment(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let updateAppointmentByID = async (id, data) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateAppointmentByID(data), [id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

let getAppointmentByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAppointmentByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveAppointment = async (id, data) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveAppointment(data), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getActiveAppointmentListBySearching = async (searchFieldObject = {}, extraWhere = "") => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.searchAppointmentForActiveList(searchFieldObject, extraWhere), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getDeactiveAppointmentListBySearching = async (searchFieldObject = {}, extraWhere = "") => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.searchAppointmentForDeactiveList(searchFieldObject, extraWhere), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getSalesPersonWiseAppointmentListByDate = async (from_date = "'2002-1-1'", to_date = 0) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSalesPersonWiseAppointmentList(from_date, to_date), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getSalesPersonWiseCompleteAppointmentListByDate = async (from_date = "'2002-1-1'", to_date = 0) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSalesPersonWiseCompleteAppointmentList(from_date, to_date), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getAppointmentDetailsByIDandClientType = async (id, client_type) => {

    if (client_type != 0) {
        return new Promise((resolve, reject) => {
            connectionIntracoLPG.query(quaries.getDetailsForNonCorporateByIDandType(id, client_type), (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        });
    } else if (client_type == 0) {
        return new Promise((resolve, reject) => {
            connectionIntracoLPG.query(quaries.getDetailsForCorporateByIDandType(id, client_type), (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        });
    }

}




module.exports = {
    getAppointmentList,
    getDeactiveAppointmentList,
    addNewAppointment,
    updateAppointmentByID,
    getAppointmentByID,
    deactiveAppointment,
    getActiveAppointmentListBySearching,
    getDeactiveAppointmentListBySearching,
    getSalesPersonWiseAppointmentListByDate,
    getSalesPersonWiseCompleteAppointmentListByDate,
    getAppointmentDetailsByIDandClientType

}