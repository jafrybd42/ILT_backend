const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDiscussionTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAllList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDiscussionTypeAllList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewDiscussionType = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addDiscussionType(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDiscussionTypeByType = async (type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDiscussionTypeByTypeName(), [type], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveDiscussionType = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveDiscussionType(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let activeDiscussionType = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.activeDiscussionType(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateDiscussionTypeByID = async (id, updateData) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateDiscussionTypeById(updateData), [id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

let getDiscussionTypeByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDiscussionTypeByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDeactiveDiscussionTypeList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDeactiveDiscussionTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getActiveDiscussionTypeList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getActiveDiscussionTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getList,
    getAllList,
    addNewDiscussionType,
    getDiscussionTypeByType,
    deactiveDiscussionType,
    activeDiscussionType,
    updateDiscussionTypeByID,
    getDiscussionTypeByID,
    getDeactiveDiscussionTypeList,
    getActiveDiscussionTypeList
}