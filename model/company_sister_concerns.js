const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');

let getAllList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAllSisterConcernList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}
let getActiveSisterConcernList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getActiveSisterConcernList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDeactiveSisterConcernList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDeactiveSisterConcernList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let addNewSisterConcern = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addSisterConcern(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getSisterConcernByID = async (sisterConcertID) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSisterConcernByID(), [sisterConcertID], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getSisterConcernAndCompanyByID = async (sisterConcernID) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSisterConcernAndCompanyByID(), [sisterConcernID], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateCompanySisterConcernByID = async (id, updateData) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateCompanySisterConcernByID(updateData), [id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}


let getSisterConcernByTitleAndID = async (company_id,title) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSisterConcernByTitleAndID(), [company_id,title], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getSisterConcernListBySearchingWithCompanyID = async (company_id,sister_concern_title) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSisterConcernListBySearchingWithCompanyID(company_id,sister_concern_title), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
            
        });
    });
    
}

let getSisterConcernListBySearchingWithCompanyIDAndSalesPersonID = async (sales_person_id,company_id,sister_concern_title) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSisterConcernListBySearchingWithCompanyIDAndSalesPersonID(sales_person_id,company_id,sister_concern_title), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
            
        });
    });
    
}

let getSisterConcernByCompanyID = async (company_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSisterConcernByCompanyID(company_id), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getSisterConcernByCompanyIDAndSalesPersonID = async (sales_person_id,company_id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSisterConcernByCompanyIDAndSalesPersonID(sales_person_id,company_id), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveSisterConcern = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveSisterConcern(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let activeSisterConcern = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.activeSisterConcern(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getSisterConcernListBySearcing = async (searchField) => {

	if (searchField.company_title !== 0 && searchField.sister_concern_title !==  0)
	{
		return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSisterConcernListBySearching(searchField),  (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
    }
    else if(searchField.company_title === 0 && searchField.sister_concern_title !== 0)
    {
        return new Promise((resolve, reject) => {
            connectionIntracoLPG.query(quaries.getSisterConcernListBySearchingWithSisterConcern(searchField),  (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        });
    }
    else if(searchField.company_title !== 0 && searchField.sister_concern_title === 0)
    {
        return new Promise((resolve, reject) => {
            connectionIntracoLPG.query(quaries.getSisterConcernListBySearchingWithCompany(searchField),  (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        });
    }
	
    
}


let getSisterConcernListBySalespersonId = async (salesperson_id) => {  // Get sister sister concern List By Salesperson
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSisterConcernListBySalespersonId(), [salesperson_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getAllList,
    getActiveSisterConcernList,
    getDeactiveSisterConcernList,
    addNewSisterConcern,
    getSisterConcernByTitleAndID,
    getSisterConcernByID,
    getSisterConcernAndCompanyByID,
    updateCompanySisterConcernByID,
    getSisterConcernByCompanyID,
    getSisterConcernByCompanyIDAndSalesPersonID,
    deactiveSisterConcern,
    activeSisterConcern,
    getSisterConcernListBySearcing,
    getSisterConcernListBySearchingWithCompanyID,
    getSisterConcernListBySearchingWithCompanyIDAndSalesPersonID,
    getSisterConcernListBySalespersonId
    
}