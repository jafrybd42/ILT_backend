const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getQuestionList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getPasswordRecoveryList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getQuestionList,
}