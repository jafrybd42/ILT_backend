const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getVehicleTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getAllList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getVehicleTypeAllList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewVehicleType = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addVehicleType(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getVehicleTypeByType = async (type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getVehicleTypeByTypeName(), [type], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveVehicleType = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveVehicleType(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let activeVehicleType = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.activeVehicleType(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateVehicleTypeByID = async (id, type) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateVehicleTypeById(), [type, id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

let getVehicleTypeByID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getVehicleTypeByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDeactiveVehicleTypeList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getDeactiveVehicleTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getActiveVehicleTypeList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getActiveVehicleTypeList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getList,
    getAllList,
    addNewVehicleType,
    getVehicleTypeByType,
    deactiveVehicleType,
    activeVehicleType,
    updateVehicleTypeByID,
    getVehicleTypeByID,
    getDeactiveVehicleTypeList,
    getActiveVehicleTypeList
}