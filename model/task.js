const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


//Promices Method
let getIncompleteTaskListById = async (salesPersonId) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getIncompleteTaskListById(),[salesPersonId], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewTask = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.addTask(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

// let getTaskByName = async (name) => {
//     return new Promise((resolve, reject) => {
//         connectionIntracoLPG.query(quaries.getTaskByName(), [name], (error, result, fields) => {
//             if (error) reject(error)
//             else resolve(result)
//         });
//     });
// }

// let deactiveTask = async (id) => {
//     return new Promise((resolve, reject) => {
//         connectionIntracoLPG.query(quaries.deactiveTask(), [id], (error, result, fields) => {
//             if (error) reject(error)
//             else resolve(result)
//         });
//     });
// }

// let activeTask = async (id) => {
//     return new Promise((resolve, reject) => {
//         connectionIntracoLPG.query(quaries.deactiveTask(), [id], (error, result, fields) => {
//             if (error) reject(error)
//             else resolve(result)
//         });
//     });
// }

// let updateTaskByID = async (id, name) => {
//     return new Promise((resolve, reject) => {
//         connectionIntracoLPG.query(quaries.updateTaskById(), [name, id], (error, result, fields) => {
//             if (error) reject(error);
//             else resolve(result);
//         });
//     });
// }



let updateTaskByID = async (id, data) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateTaskByID(data), [id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}


let getTaskByID = async (id, userType = 0, salesPersonId = 0) => {

    if (userType == 1 || userType == 2 || userType == 3) { // admin can get all data

        return new Promise((resolve, reject) => {
            connectionIntracoLPG.query(quaries.getTaskByID(), [id], (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        });

    } else if (userType == 4) { // only sales person get her/his task

        return new Promise((resolve, reject) => {
            connectionIntracoLPG.query(quaries.getTaskByIDAndSalespersonId(), [id, salesPersonId], (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        });

    } else return [];

}

let getTaskListByClientID = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getTaskByClientId(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getTaskListByClientIDAndSalesPersonId = async (clientId, salesPersonId) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getTaskByClientIdAndSalesPersonId(), [clientId, salesPersonId], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getTaskListBySearching = async (searchFieldObject = {}, extraWhere = "") => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.searchTask(searchFieldObject, extraWhere), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}

let reassignTask = async (present_saler_id, new_saler_id, isAlltaskAssign = true, taskList = []) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.reassignTask(isAlltaskAssign, taskList), [new_saler_id, present_saler_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getProspectTypeWiseTaskCount = async (prospectTypeList, salesPersonId = 0) => {
    return new Promise((resolve, reject) => {
        if(salesPersonId == 0){
            connectionIntracoLPG.query(quaries.getProspectTypeWiseCount(prospectTypeList, 0), (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        } else {
            connectionIntracoLPG.query(quaries.getProspectTypeWiseCount(prospectTypeList, 1), salesPersonId, (error, result, fields) => {
                if (error) reject(error)
                else resolve(result)
            });
        }
        
    });
}


let getSalesPersonWiseTaskListByDate = async (from_date = "'2002-1-1'", to_date = 0) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSalesPersonWiseTaskList(from_date, to_date), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getSalesPersonWiseCompleteTaskListByDate = async (from_date = "'2002-1-1'", to_date = 0) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSalesPersonWiseCompleteTaskList(from_date, to_date), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


module.exports = {
    addNewTask,
    updateTaskByID,
    getTaskByID,
    getTaskListByClientID,
    getTaskListBySearching,
    reassignTask,
    getIncompleteTaskListById,
    getTaskListByClientIDAndSalesPersonId,
    getProspectTypeWiseTaskCount,
    getSalesPersonWiseTaskListByDate,
    getSalesPersonWiseCompleteTaskListByDate
}