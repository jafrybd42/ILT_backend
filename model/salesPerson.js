const connectionIntracoLPG = require('../connection/connection').connectionIntracoLPG;
const quaries = require('../query/lpg');


// Promices Method
let getSalesPersonList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSalesPersonList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
} 

let getAllSalesPersonList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getAllSalesPersonList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
} 

let getSalesPersonDeleteList = async () => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSalesPersonDeleteList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
} 

let addNewSalesPerson = async (info) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.registerSalesPersonAccount(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deactiveAccount = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.deactiveSalesPersonAccount(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getProfileById = async (id) => { // search by active salesperson
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSalesPersonProfileById(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getProfileById2 = async (id) => { // search by active and deactive salesperson
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSalesPersonProfileByIdAllType(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getProfileByPhoneNumber = async (phone_number) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getProfileByPhoneNumber(), [phone_number], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getSalesPersonProfileByIdForOthers = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getSalesPersonProfileByIdForOthers(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getTaskCountBySalespersonId = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getTaskCountBySalespersonId(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateProfileById = async (id, data) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateSalesPersonProfileById(data), [id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}


let getPasswordRecoveryQuestionId = async (id) => {
    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.getPasswordRecoveryQuestionId(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

module.exports = {
    getSalesPersonList,
    getAllSalesPersonList,
    addNewSalesPerson,
    deactiveAccount,
    getProfileById,  // search by active salesperson
    getProfileById2, // search by active and deactive salesperson
    getProfileByPhoneNumber,
    updateProfileById,
    getSalesPersonProfileByIdForOthers,
    getTaskCountBySalespersonId,
    getPasswordRecoveryQuestionId,
    getSalesPersonDeleteList
}