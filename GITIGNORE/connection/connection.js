const mysql = require('mysql');

const connectionIntracoLPG = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "intraco_lpg"
});


module.exports = {
    connectionIntracoLPG
}