var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
const isEmpty = require("is-empty");
const superAdmin_model = require('../../model/superAdmin');
const subAdmin_model = require('../../model/subAdmin');
const salesPerson_model = require('../../model/salesPerson');

router.use(async function (req, res, next) {
    var token = req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, global.config.secretKey,
            {
                algorithm: global.config.algorithm

            }, async function (err, decoded) {
                if (err) {
                    let errordata = {
                        message: err.message,
                        expiredAt: err.expiredAt
                    };
                    return res.send({
                        "error": true,
                        "message": "Timeout Login Fast"
                    });
                }

                // Check User account is active or not.
                let profileData;
                if (decoded.user_type == 2) {
                    profileData = await superAdmin_model.getProfileById(decoded.id);
                } else if (decoded.user_type == 3) {
                    profileData = await subAdmin_model.getProfileById(decoded.id);
                } else if (decoded.user_type == 4) {
                    profileData = await salesPerson_model.getProfileById(decoded.id);
                }


                if (decoded.user_type != 1) {
                    if (profileData === undefined || isEmpty(profileData) || profileData[0].active_status !== 0) {
                        return res.send({
                            "error": true,
                            "message": "Your account is Delete. You can't access this System."
                        });
                    }
                }



                req.decoded = decoded;
                next();
            });
    } else {
        return res.send({
            "error": true,
            "message": "Unauthorize Request"
        });
    }
});

module.exports = router;