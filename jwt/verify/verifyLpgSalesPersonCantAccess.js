var express = require('express');
var router = express.Router();

router.use(function (req, res, next) {

    const userData = req.decoded;
    // console.log(userData);

    if (userData === undefined || userData.user_type == 4) {
        return res.send({
            "error": true,
            "message": "You are not eligible on this route."
        });
    } else next();
});

module.exports = router;