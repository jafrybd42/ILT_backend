const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const leads_model = require('../model/leads');

const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');
const {
    response
} = require("express");



router.get('/list', verifyToken, async (req, res) => { /// Active Leads

    let result = await leads_model.getActiveList();

    return res.send({
        "error": false,
        "message": "Lead name List.",
        "data": result
    });
});


router.get('/allList', verifyToken, async (req, res) => {

    let result = await leads_model.getAllList();

    return res.send({
        "error": false,
        "message": "Lead Type  List.",
        "data": result
    });
});



router.get('/deactiveList', verifyToken, async (req, res) => {

    let result = await leads_model.getDeactiveList();

    return res.send({
        "error": false,
        "message": "Deactive Lead List.",
        "data": result
    });
});

router.get('/activeList', verifyToken, async (req, res) => {

    let result = await leads_model.getActiveList();

    return res.send({
        "error": false,
        "message": "Active Lead List.",
        "data": result
    });
});


router.post('/add', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let reqData = {
        "name": req.body.name,
        "active_status": 0
    }


    if (reqData.name == undefined || reqData.name.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Lead name and length must then 1.",
        });
    }

    let existingData = await leads_model.getLeadByName(reqData.name);

    if (!isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Lead name Already Exist." : "Lead name Exist but Deactivate, Please contect to Admin."
        });
    }

    let result = await leads_model.addNewLead(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Lead name Added."
    });
});

router.post(['/deactive', '/delete'], [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await leads_model.getLeadByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Lead ID Not Exist."
        });
    }

    let result = await leads_model.deactiveLead(id);

    return res.send({
        "error": false,
        "message": `Lead '${existingData[0].name}' Delete Successfully Done.`

    });
});


router.post('/active', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await leads_model.getLeadByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Lead ID Not Exist."
        });
    }

    let result = await leads_model.activeLead(id);

    return res.send({
        "error": false,
        "message": "Lead Name Activated."

    });
});


router.post('/update', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let name = req.body.name;
    let id = req.body.id;

    if (name == undefined || name.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Lead name and length must then 1.",
        });
    }

    let existingData = await leads_model.getLeadByName(name);

    if (!isEmpty(existingData) && existingData[0].id !== id) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Lead name Already Exist." : "Lead name Exist but Deactivate, Please contect to Admin."
        });
    }

    existingData = await leads_model.getLeadByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Lead ID Not Exist."
        });
    }


    let result = await leads_model.updateLeadByID(id, name);

    if (result.affectedRows == undefined || result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Lead Update Successfully Done."
    });

});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


module.exports = router;