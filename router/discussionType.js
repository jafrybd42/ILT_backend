const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const discussion_type_model = require('../model/discussionType');

const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');
const e = require("express");

router.get('/list', verifyToken, async (req, res) => { /// Active Discussion Type

    let result = await discussion_type_model.getActiveDiscussionTypeList();

    return res.send({
        "error": false,
        "message": "Discussion type List.",
        "data": result
    });
});

router.get('/allList', verifyToken, async (req, res) => {

    let result = await discussion_type_model.getAllList();

    return res.send({
        "error": false,
        "message": "Discussion type List.",
        "data": result
    });
});


router.get('/deactiveList', verifyToken, async (req, res) => {

    let result = await discussion_type_model.getDeactiveDiscussionTypeList();

    return res.send({
        "error": false,
        "message": "Deactive Discussion Type List.",
        "data": result
    });
});

router.get('/activeList', verifyToken, async (req, res) => {

    let result = await discussion_type_model.getActiveDiscussionTypeList();

    return res.send({
        "error": false,
        "message": "Active Discussion Type  List.",
        "data": result
    });
});


router.post('/add', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {


    let reqTime = {
        "service_year": Number(req.body.service_year),
        "service_month": Number(req.body.service_month),
        "service_day": Number(req.body.service_day),
        "reminder_year": Number(req.body.reminder_year),
        "reminder_month": Number(req.body.reminder_month),
        "reminder_day": Number(req.body.reminder_day),

    }


    if (isNaN(reqTime.service_year) || reqTime.service_year == null) {
        return res.send({
            "error": true,
            "message": "Please Give Valid Number in Service Year.",
        });
    }

    if (isNaN(reqTime.service_month) || reqTime.service_month == null) {
        return res.send({
            "error": true,
            "message": "Please Give Valid Number in Service Month.",
        });
    }

    if (reqTime.service_month > 11) {
        return res.send({
            "error": true,
            "message": "Service Month Should not be Greater than 11.If it is 12 ,Please Select Year",
        });
    }

    if (isNaN(reqTime.service_day) || reqTime.service_day == null) {
        return res.send({
            "error": true,
            "message": "Please Give Valid Number in Service Day",
        });
    }

    if (reqTime.service_day > 30) {
        return res.send({
            "error": true,
            "message": "Service Day Should not be Greater than 30.If it is 31 ,Please Select Month",
        });
    }

    if (reqTime.service_year == 0 && reqTime.service_month == 0 && reqTime.service_day == 0) {
        return res.send({
            "error": true,
            "message": "You have to select At Least one field from Service Day or Service Month or Service Year",
        });
    }

    if (isNaN(reqTime.reminder_year) || reqTime.reminder_year == null) {
        return res.send({
            "error": true,
            "message": "Please Give Valid Number in Reminder Year",
        });
    }


    if (isNaN(reqTime.reminder_month) || reqTime.reminder_month == null) {
        return res.send({
            "error": true,
            "message": "Please Give Valid Number in Reminder Month",
        });
    }

    if (reqTime.reminder_month > 11) {
        return res.send({
            "error": true,
            "message": "Reminder Month Should not be Greater than 11.If it is 12 ,Please Select Year",
        });
    }


    if (isNaN(reqTime.reminder_day) || reqTime.reminder_day == null) {
        return res.send({
            "error": true,
            "message": "Please Give Valid Number in Reminder Day",
        });
    }

    if (reqTime.reminder_day > 30) {
        return res.send({
            "error": true,
            "message": "Reminder Day Should not be Greater than 30.If it is 31 ,Please Select Month",
        });
    }


    if (reqTime.reminder_year == 0 && reqTime.reminder_month == 0 && reqTime.reminder_day == 0) {
        return res.send({
            "error": true,
            "message": "You have to select At Least one field from Reminder Day or Reminder Month or Reminder Year",
        });
    }

    if (reqTime.service_year < reqTime.reminder_year) {
        return res.send({
            "error": true,
            "message": "Service Year Should be greater than Reminder Year",
        });
    } else if (reqTime.service_year == reqTime.reminder_year) {

        if (reqTime.service_month < reqTime.reminder_month) {
            return res.send({
                "error": true,
                "message": "Service Month Should be greater than Reminder Month",
            });
        } else if (reqTime.service_month == reqTime.reminder_month) {
            if (reqTime.service_day < reqTime.reminder_day) {
                return res.send({
                    "error": true,
                    "message": "Service Day Should be greater than Reminder Day",
                });
            } else if (reqTime.service_day == reqTime.reminder_day) {
                return res.send({
                    "error": true,
                    "message": "Service Day and  Reminder Day Should Not be Same",
                });
            }

        }
    }


    let reqData = {
        "type": req.body.type,
        "service_time_period": reqTime.service_year + '_' + reqTime.service_month + '_' + reqTime.service_day,
        "reminder_time_period": reqTime.reminder_year + '_' + reqTime.reminder_month + '_' + reqTime.reminder_day,
        "active_status": 0
    }


    if (reqData.type == undefined || reqData.type.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Discussion Type and length must then 1.",
        });
    }

    let existingData = await discussion_type_model.getDiscussionTypeByType(reqData.type);

    if (!isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Discussion type Already Exist." : "Discussion type Exist but Deactivate, Please contact to Admin."
        });
    }

    let result = await discussion_type_model.addNewDiscussionType(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Discussion type Added."
    });
});

router.post('/delete', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await discussion_type_model.getDiscussionTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Discussion type ID Not Exist."
        });
    }

    let result = await discussion_type_model.deactiveDiscussionType(id);

    return res.send({
        "error": false,
        "message": `Discussion type '${existingData[0].type}' Deleted Successfully .`,
        "data": []
    });
});

router.post('/active', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await discussion_type_model.getDiscussionTypeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Discussion Type  Not Exist."
        });
    }


    if (existingData[0].active_status === 0) {
        return res.send({
            "error": true,
            "message": "Already this is Active"
        });
    }
    let result = await discussion_type_model.activeDiscussionType(id);

    return res.send({
        "error": false,
        "message": "Discussion Type Activated."

    });
});

router.post('/update', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {


    let id = req.body.id;
    let cureentData = await discussion_type_model.getDiscussionTypeByID(id);
    if (isEmpty(cureentData)) {
        return res.send({
            "error": true,
            "message": "Discussion type ID Not Exist."
        });
    }

    let service_data = cureentData[0].service_time_period;
    let reminder_data = cureentData[0].reminder_time_period;

    var [s_year, s_month, s_day] = service_data.split('_');
    var [r_year, r_month, r_day] = reminder_data.split('_');


    let requestedTime = {
        "service_year": req.body.service_year,
        "service_month": req.body.service_month,
        "service_day": req.body.service_day,
        "reminder_year": req.body.reminder_year,
        "reminder_month": req.body.reminder_month,
        "reminder_day": req.body.reminder_day,

    }

    let updateDataForTime = {
        "service_year": s_year,
        "service_month": s_month,
        "service_day": s_day,
        "reminder_year": r_year,
        "reminder_month": r_month,
        "reminder_day": r_day,

    }

    if (s_year != requestedTime.service_year) {
        if ((requestedTime.service_year == undefined  || isNaN(Number(requestedTime.service_year)))) {
            return res.send({
                "error": true,
                "message": "Please Give Valid Number in Service Year."
            });
        } else if (!(requestedTime.service_year == undefined || isNaN(Number(requestedTime.service_year)))) {
            updateDataForTime.service_year = requestedTime.service_year;
        } else {
            updateDataForTime.service_year = 0;
        }
    }



    if (s_month != requestedTime.service_month) {
        if (isNaN(Number(requestedTime.service_month))) {
            return res.send({
                "error": true,
                "message": "Please Give Valid Number in Service Month."
            });
        } else if (requestedTime.service_month > 11) {
            return res.send({
                "error": true,
                "message": "Service Month Should not be Greater than 11.If it is 12 ,Please Select Year",
            });
        } else {
            updateDataForTime.service_month = requestedTime.service_month;
        }
    }


    if (s_day != requestedTime.service_day) {
        if (isNaN(Number(requestedTime.service_day))) {
            return res.send({
                "error": true,
                "message": "Please Give Valid Number in Service Day."
            });
        } else if (requestedTime.service_day > 30) {
            return res.send({
                "error": true,
                "message": "Service Day Should not be Greater than 30.If it is 31 ,Please Select Month",
            });
        } else {
            updateDataForTime.service_day = requestedTime.service_day;
        }
    }


    if (updateDataForTime.service_year == 0 && updateDataForTime.service_day == 0 && updateDataForTime.service_month == 0) {
        return res.send({
            "error": true,
            "message": "You have to select At Least one field from Service Day or Service Month or Service Year",
        });
    }

    if (r_year != requestedTime.reminder_year) {
        if ((requestedTime.reminder_year == undefined || isNaN(Number(requestedTime.reminder_year)))) {
            return res.send({
                "error": true,
                "message": "Please Give Valid Number in Reminder Year."
            });
        } else if (!(requestedTime.reminder_year == undefined || isNaN(Number(requestedTime.reminder_year)))) {
            updateDataForTime.reminder_year = requestedTime.reminder_year;
        } else {
            updateDataForTime.reminder_year = 0;
        }

    }


    if (r_month != requestedTime.reminder_month) {
        if (isNaN(Number(requestedTime.reminder_month))) {
            return res.send({
                "error": true,
                "message": "Please Give Valid Number in Reminder Month."
            });
        } else if (requestedTime.reminder_month > 11) {
            return res.send({
                "error": true,
                "message": "Reminder Month Should not be Greater than 11.If it is 12 ,Please Select Year",
            });
        } else {
            updateDataForTime.reminder_month = requestedTime.reminder_month;
        }

    }

    if (r_day != requestedTime.reminder_day) {
        if (isNaN(Number(requestedTime.reminder_day))) {
            return res.send({
                "error": true,
                "message": "Please Give Valid Number in Reminder Day."
            });
        } else if (requestedTime.reminder_day > 30) {
            return res.send({
                "error": true,
                "message": "Reminder Day Should not be Greater than 30.If it is 31 ,Please Select Month",
            });
        } else {
            updateDataForTime.reminder_day = requestedTime.reminder_day;
        }
    }

    if (updateDataForTime.reminder_year == 0 && updateDataForTime.reminder_month == 0 && updateDataForTime.reminder_day == 0) {
        return res.send({
            "error": true,
            "message": "You have to select At Least one field from Reminder Day or Reminder Month or Reminder Year",
        });
    }

    if (updateDataForTime.service_year < updateDataForTime.reminder_year) {
        return res.send({
            "error": true,
            "message": "Service Year Should be greater than Reminder Year",
        });
    } else if (updateDataForTime.service_year == updateDataForTime.reminder_year) {

        if (updateDataForTime.service_month < updateDataForTime.reminder_month) {
            return res.send({
                "error": true,
                "message": "Service Month Should be greater than Reminder Month",
            });
        } else if (updateDataForTime.service_month == updateDataForTime.reminder_month) {
            if (updateDataForTime.service_day < updateDataForTime.reminder_day) {
                return res.send({
                    "error": true,
                    "message": "Service Day Should be greater than Reminder Day",
                });
            } else if (updateDataForTime.service_day == updateDataForTime.reminder_day) {
                return res.send({
                    "error": true,
                    "message": "Service Day and  Reminder Day Should Not be Same",
                });
            }

        }
    }

    let updateData = {
        "service_time_period": updateDataForTime.service_year + '_' + updateDataForTime.service_month + '_' + updateDataForTime.service_day,
        "reminder_time_period": updateDataForTime.reminder_year + '_' + updateDataForTime.reminder_month + '_' + updateDataForTime.reminder_day,
    }


    if (req.body.type) {
        if (cureentData[0].type != req.body.type) {
            if (req.body.type == undefined || req.body.type.length < 2) {
                return res.send({
                    "error": true,
                    "message": "Need valid Discussion Type and length must then 1.",
                });
            }
            let existingData = await discussion_type_model.getDiscussionTypeByType(req.body.type);

            if (!isEmpty(existingData) && existingData[0].id !== id) {
                return res.send({
                    "error": true,
                    "message": existingData[0].active_status == "0" ? "Discussion type Already Exist." : "Discussion type Exist but Deactivate, Please contact to Admin."
                });
            }
            updateData.type = req.body.type;
        }
    } else {
        updateData.type = cureentData[0].type;
    }

    var currentdate = new Date();

    updateData.updated_at = currentdate.getFullYear() + "-" + currentdate.getMonth() + 1 + "-" + currentdate.getDate() +
        " " + currentdate.getHours() + ":" +
        currentdate.getMinutes() + ":" + currentdate.getSeconds();



    let result = await discussion_type_model.updateDiscussionTypeByID(id, updateData);

    if (result.affectedRows == undefined || result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Discussion Update Successfully Done."
    });

});

router.get('/details/:id', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    const id = req.params.id;

    const existingInfo = await discussion_type_model.getDiscussionTypeByID(id);

    if (isEmpty(existingInfo)) {
        return res.send({
            "error": true,
            "message": "Discussion Type Not Exist."
        });
    } else if (existingInfo[0].active_status == 1) {
        return res.send({
            "error": true,
            "message": "Discussion Type is not Activated."
        });
    } else {

        return res.send({
            "error": false,
            "message": " Found.",
            "data": existingInfo
        });
    }


});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


module.exports = router;