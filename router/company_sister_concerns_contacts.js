const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const sales_person_model = require('../model/salesPerson');
const companies_model = require('../model/companies');
const company_sister_concerns_model = require('../model/company_sister_concerns');
const company_sister_concerns_contacts_model = require('../model/company_sister_concerns_contacts');
const access_corporate_client_model = require('../model/access_corporate_clients');
const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');
const verifySuperAdminCantAccess = require('../jwt/verify/verifyLpgSuperAdminCantAccess');
const verifySubAdminCantAccess = require('../jwt/verify/verifyLpgSubAdminCantAccess');
const e = require("express");



router.get('/:id/contactList', verifyToken, async (req, res) => {

    let company_sister_concern_id = req.params.id;
    let sales_person_id = req.decoded.id;

    let sister_concern_access_details = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(company_sister_concern_id, sales_person_id);

    if (isEmpty(sister_concern_access_details)) {
        return res.send({
            "error": true,
            "message": "This is not your company Sister concern."
        });
    }


    let result = await company_sister_concerns_contacts_model.getContactList(company_sister_concern_id);

    return res.send({
        "error": false,
        "message": "All Contact List",
        "count": result.length,
        "data": result
    });
});



router.post('/:id/addNewContactPerson', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let reqData = {
        "company_sister_concern_id": req.params.id,
        "name": req.body.name,
        "phone_no_primary": req.body.phone_no_primary,
        "phone_no_secondary": req.body.phone_no_secondary,
        "rank": req.body.rank
    }



    let sales_person_id = req.decoded.id;

    let errorMessage = "";
    let isError = 0;

    /* let sisterConcernData = await company_sister_concerns_model.getSisterConcernByID(reqData.company_sister_concern_id);
    
    if (reqData.company_sister_concern_id == undefined ||  isEmpty(sisterConcernData) || sisterConcernData[0].active_status == 1) {

        isError = 1;
        errorMessage += "Invalid Company Sister Concern ";
       
    } */

    let sister_concern_access_details = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(reqData.company_sister_concern_id, sales_person_id);

    if (isEmpty(sister_concern_access_details)) {
        return res.send({
            "error": true,
            "message": "This is not your company Sister concern."
        });
    }

    if (reqData.name == undefined || reqData.name.length < 2) {
        isError = 1;
        errorMessage += "Need valid name and length At Least 2. ";

    }

    if (reqData.phone_no_primary == undefined || reqData.phone_no_primary.length != 11) {
        isError = 1;
        errorMessage += "Primary Phone number invalid.";
    }

    if (reqData.phone_no_secondary == undefined || reqData.phone_no_secondary == "") {
        reqData.phone_no_secondary = "";
    }

    if (reqData.phone_no_secondary !== undefined && reqData.phone_no_secondary.length != 11 && reqData.phone_no_secondary != "") {
        isError = 1;
        errorMessage += "Secondary Phone number invalid.";
    }

    if (reqData.rank == undefined || reqData.rank == "") {
        reqData.rank = "";
    }

    if (reqData.rank !== undefined && reqData.rank.length < 2 && reqData.rank != "") {
        isError = 1;
        errorMessage += "Need valid Designation and length At Least 2.";
    }


    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }



    let result = await company_sister_concerns_contacts_model.addNewContactPerson(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }


    return res.send({
        "error": false,
        "message": "Contact Person Added Successfully."
    });


});


router.post('/editContactPerson', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id,
        //"company_sister_concern_id": req.body.company_sister_concern_id,
        "name": req.body.name,
        "phone_no_primary": req.body.phone_no_primary,
        "phone_no_secondary": req.body.phone_no_secondary,
        "rank": req.body.rank
    }
    let sales_person_id = req.decoded.id;

    let updateData = {};

    let errorMessage = "";
    let isError = 0; // 0 = No
    let willWeUpdate = 1; // 0 = yes;

    const existingPersonInfo = await company_sister_concerns_contacts_model.getContactDetailsByID(updateRequestData.id);

    if (isEmpty(existingPersonInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }
    let company_sister_concern_id = existingPersonInfo[0].company_sister_concern_id;

    let sister_concern_access_details = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(company_sister_concern_id, sales_person_id);

    if (isEmpty(sister_concern_access_details)) {
        return res.send({
            "error": true,
            "message": "This is not your company Sister concern."
        });
    }

    if (existingPersonInfo[0].name !== updateRequestData.name) {
        if (updateRequestData.name == undefined || updateRequestData.name.length < 2) {
            isError = 1;
            errorMessage += "Need valid name and length At Least 2. ";

        } else {
            willWeUpdate = 0;
            updateData.name = updateRequestData.name;

        }
    }

    if (existingPersonInfo[0].phone_no_primary !== updateRequestData.phone_no_primary) {

        if (updateRequestData.phone_no_primary == undefined || updateRequestData.phone_no_primary.length != 11) {
            isError = 1;
            errorMessage += "Primary Phone number invalid.";
        } else {
            willWeUpdate = 0;
            updateData.phone_no_primary = updateRequestData.phone_no_primary;
        }
    }

    if (existingPersonInfo[0].phone_no_secondary !== updateRequestData.phone_no_secondary) {

        if (updateRequestData.phone_no_secondary == undefined || updateRequestData.phone_no_secondary == "") {
            willWeUpdate = 0;
            updateData.rank = existingPersonInfo[0].phone_no_secondary;
        } else if (updateRequestData.phone_no_secondary !== undefined && updateRequestData.phone_no_secondary.length != 11 && updateRequestData.phone_no_secondary != "") {
            isError = 1;
            errorMessage += "Secondary Phone number invalid.";
        } else {
            willWeUpdate = 0;
            updateData.phone_no_secondary = updateRequestData.phone_no_secondary;
        }
    }

    if (existingPersonInfo[0].rank !== updateRequestData.rank) {
        if (updateRequestData.rank == undefined || updateRequestData.rank == "") {
            willWeUpdate = 0;
            updateData.rank = existingPersonInfo[0].rank;
        } else if (updateRequestData.rank !== undefined && updateRequestData.rank.length < 2 && updateRequestData.rank !== "") {
            isError = 1;
            errorMessage += "Need valid Designation and length At Least 2.";
        } else {
            willWeUpdate = 0;
            updateData.rank = updateRequestData.rank;

        }
    }
    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let updateResult = await company_sister_concerns_contacts_model.updateContactPersonByID(updateRequestData.id, updateData);

    if (updateResult.affectedRows == undefined || updateResult.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }


    return res.send({
        "error": false,
        "message": "Contact Person Info Updated Successfully !!!."
    });
});



router.post(['/deactive', '/delete'], [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let id = req.body.id;
    let sales_person_id = req.decoded.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await company_sister_concerns_contacts_model.getContactDetailsByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "No Data Exists"
        });
    }

    let company_sister_concern_id = existingData[0].company_sister_concern_id;

    let sister_concern_access_details = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(company_sister_concern_id, sales_person_id);

    if (isEmpty(sister_concern_access_details)) {
        return res.send({
            "error": true,
            "message": "This is not your company Sister concern."
        });
    }



    let result = await company_sister_concerns_contacts_model.deleteContactPerson(id);

    return res.send({
        "error": false,
        "message": `Contact Person '${existingData[0].name}' Deleted!!!`

    });
});






router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

module.exports = router;