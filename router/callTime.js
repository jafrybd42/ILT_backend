const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const call_time_model = require('../model/callTime');

const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');

router.get('/list', verifyToken, async (req, res) => {

    let result = await call_time_model.getList();

    return res.send({
        "error": false,
        "message": "Call Time type List.",
        "data": result
    });
});






router.post('/add', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let reqData = {
        "type": req.body.type,
        "active_status": 0
    }


    if (reqData.type == undefined || reqData.type.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Call Time Type and length must then 1.",
        });
    }

    let existingData = await call_time_model.getCallTimeByType(reqData.type);

    if (!isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Call Time type Already Exist." : "Call Time type Exist but Deactivate, Please contect to Admin."
        });
    }

    let result = await call_time_model.addNewCallTime(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Call Time type Added."
    });
});

router.post('/delete', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let existingData = await call_time_model.getCallTimeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Call Time ID Not Exist."
        });
    }

    let result = await call_time_model.deactiveCallTime(id);

    return res.send({
        "error": false,
        "message": `Call Time type '${existingData[0].type}' Deleted Successfully .`,
        "data": []
    });
});

router.post('/update', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let type = req.body.type;
    let id = req.body.id;

    if (type == undefined || type.length < 2) {
        return res.send({
            "error": true,
            "message": "Need valid Call Time Type and length must then 1.",
        });
    }

    let existingData = await call_time_model.getCallTimeByType(type);

    if (!isEmpty(existingData) && existingData[0].id !== id) {
        return res.send({
            "error": true,
            "message": existingData[0].active_status == "0" ? "Call Time type Already Exist." : "Call Time type Exist but Deactivate, Please contect to Admin."
        });
    }

    existingData = await call_time_model.getCallTimeByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "error": true,
            "message": "Call Time ID Not Exist."
        });
    }


    let result = await call_time_model.updateCallTimeByID(id, type);

    if (result.affectedRows == undefined || result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    return res.send({
        "error": false,
        "message": "Call Time Update Successfully Done."
    });

});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});


module.exports = router;