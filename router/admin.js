const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const bcrypt = require('bcrypt');
const superAdmin_model = require('../model/superAdmin');
const subAdmin_model = require('../model/subAdmin');
const common_model = require('../model/common');
const verifyToken = require('../jwt/verify/verifyLpgToken');
const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');
const verifySubAdminCantAccess = require('../jwt/verify/verifyLpgSubAdminCantAccess');




//  ------------ Super Admin -----------
router.post('/superAdmin/registration', [verifyToken, verifySalesPersonCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let userData = {
        "password": req.body.password,
        "phone_number": req.body.phone,
        "department_id": 1,
        "user_type": 2,
        "profile_id": 0
    }
    let extraUserInfo = {
        "name": req.body.name,
        "email": req.body.email
    };

    let errorMessage = "";
    let isError = 0;

    if (extraUserInfo.name == undefined || extraUserInfo.name.length < 2) {
        isError = 1;
        errorMessage = "Need valid name and length getter then 2. ";
    }

    if (userData.password == undefined || userData.password.length < 6) {
        isError = 1;
        errorMessage += "Need valid password and length getter then 6. ";
    }

    if (userData.phone_number == undefined || userData.phone_number.length != 11) {
        isError = 1;
        errorMessage += "Phone number invalid.";
    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }


    let getUserData = await common_model.getUserByPhone(userData.phone_number);
    if (!isEmpty(getUserData)) {
        return res.send({
            "error": true,
            "message": "Phone number Already Use."
        });
    } else {

        let superAdminData = {
            "name": extraUserInfo.name,
            "phone_number": userData.phone_number,
            "used_phone_no": userData.phone_number,
            "email": extraUserInfo.email,
            "image": "superAdmin.jpg",
            "active_status": 0,
            "visibility": 0
        }

        let result = await superAdmin_model.addNewSuperAdmin(superAdminData);

        if (result.affectedRows == undefined || result.affectedRows < 1) {
            return res.send({
                "error": true,
                "message": "Some thing Wrong in system database."
            });
        }

        userData.profile_id = result.insertId;

        userData.password = bcrypt.hashSync(userData.password, 10); // hasing Password
        result = await common_model.addNewUser(userData);

        if (result.affectedRows == undefined || result.affectedRows < 1) {
            return res.send({
                "error": true,
                "message": "Some thing Wrong in system database."
            });
        }


        return res.send({
            "error": false,
            "message": "Supper admin successfully added.",
            "data": {
                "phone": userData.phone_number,
                "name": superAdminData.name
            }
        });
    }
});

router.get('/superAdmin/list', [verifyToken, verifySalesPersonCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let result = await superAdmin_model.getSuperAdminList();

    return res.send({
        "error": false,
        "message": "Super Admin List.",
        "data": result
    });
});

router.post('/superAdmin/delete', [verifyToken, verifySalesPersonCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (typeof (id) !== "number" || !id || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let userData = await common_model.getUserByProfileIdAndUserType(id, 2);

    if (!isEmpty(userData)) {

        if (userData[0].phone_number === "0100") {
            return res.send({
                "error": true,
                "message": "Super Admin Profile already Deleted."
            });
        }

        let superAdminProfile = await superAdmin_model.getProfileById(id);

        if (isEmpty(superAdminProfile)) {
            return res.send({
                "error": false,
                "message": "Unknown Super Admin."
            });
        }

        let result = await superAdmin_model.deactiveAccount(id);

        if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
            return res.send({
                "error": true,
                "message": "Account Delete Fail try again."
            });
        }

        result = await superAdmin_model.updateProfileById(id, { "phone_number": "0100" });

        if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
            return res.send({
                "error": true,
                "message": "Account Delete Fail try again."
            });
        }

        result = await common_model.updateUserById(userData[0].id, { "phone_number": "0100" });

        if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
            return res.send({
                "error": true,
                "message": "Account Delete Fail try again."
            });
        }


        return res.send({
            "error": false,
            "message": "Super Admin (Phone No: " + userData[0].phone_number + ", Name: " + superAdminProfile[0].name + ") Delete Successfully Done."

        });

    } else {
        return res.send({
            "error": false,
            "message": "Some thing wrong User Not found.",
            "data": []
        });
    }

});

router.post('/superAdmin/profile/update', [verifyToken, verifySalesPersonCantAccess, verifySubAdminCantAccess], async (req, res) => {

    const updateRequestData = {
        "id": req.body.id,
        "phone_number": req.body.phone,
        "name": req.body.name,
        "email": req.body.email
    }

    const existinUserInfo = await superAdmin_model.getProfileById(updateRequestData.id);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    let updateData = {};

    let errorMessage = "";
    let isError = 0; // 0 = No
    let willWeUpdate = 1; // 0 = yes;

    if (existinUserInfo[0].name !== updateRequestData.name) {

        if (updateRequestData.name == undefined || updateRequestData.name.length < 2) {
            isError = 1;
            errorMessage = "Need valid name and length getter then 2. ";
        } else {
            willWeUpdate = 0;
            updateData.name = updateRequestData.name;
        }
    }

    //use in future 
    // if (existinUserInfo[0].phone_number !== updateRequestData.phone_number) {

    //     if (updateRequestData.phone_number == undefined || updateRequestData.phone_number.length != 11) {
    //         isError = 1;
    //         errorMessage += "Phone number invalid. ";
    //     } else {

    //         let userINPhoneNumber = await common_model.getUserByPhone(updateRequestData.phone_number);

    //         if (!isEmpty(userINPhoneNumber)){
    //             isError = 1;
    //             errorMessage += "Phone number already use.";
    //         } else {
    //             willWeUpdate = 0;
    //             updateData.phone_number = updateRequestData.phone_number;
    //         }

    //     }
    // }

    if (existinUserInfo[0].email !== updateRequestData.email) {

        if (isEmpty(updateRequestData.email)) {
            willWeUpdate = 0;
            updateData.email = updateRequestData.email;
        } else {
            const re = /\S+@\S+\.\S+/;
            if (!re.test(updateRequestData.email)) {
                isError = 1;
                errorMessage += " Email is invalid.";
            } else {
                willWeUpdate = 0;
                updateData.email = updateRequestData.email;
            }
        }
    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let result = {};
    if (willWeUpdate == 0) {
        if (isEmpty(updateData)) {
            return res.send({
                "error": false,
                "message": "You are not changing data."
            });
        } else {
            result = await superAdmin_model.updateProfileById(updateRequestData.id, updateData);
        }
    }

    if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Update Successfully Done"
        });
    }

});

router.get('/superAdmin/profile/view/:id', [verifyToken, verifySalesPersonCantAccess, verifySubAdminCantAccess], async (req, res) => {

    const id = req.params.id;

    const userInfo = await superAdmin_model.getProfileById(id);

    if (isEmpty(userInfo)) {
        return res.send({
            "error": true,
            "message": "Super Admin Not Exist."
        });
    } else {
        delete userInfo[0].active_status;
        return res.send({
            "error": false,
            "message": "Super Admin Found.",
            "data": userInfo[0]
        });
    }
});


router.post('/superAdmin/profile/passwordChange', [verifyToken, verifySalesPersonCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id,
        "old_password": req.body.old_password,
        "new_password": req.body.new_password,
    }

    let existingSuperAdminInfo = await superAdmin_model.getProfileById(updateRequestData.id);

    if (isEmpty(existingSuperAdminInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    const existinUserInfo = await common_model.getUserByProfileIdAndUserType(existingSuperAdminInfo[0].id, 2);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }


    if (bcrypt.compareSync(updateRequestData.old_password, existinUserInfo[0].password)) {
        if (updateRequestData.new_password == undefined || updateRequestData.new_password.length < 6) {
            return res.send({
                "error": true,
                "message": "Give Valid Password and Length should be greater than 6."
            });
        } else {

            updateRequestData.new_password = bcrypt.hashSync(updateRequestData.new_password, 10); // hasing Password
            let result = await common_model.passwordChangeForUser(existinUserInfo[0].id, updateRequestData.new_password);

            if (!isEmpty(result) && result.affectedRows == 0) {
                return res.send({
                    "error": true,
                    "message": "Update Fail try again"
                });
            } else {
                return res.send({
                    "error": false,
                    "message": "Update Successfully Done"
                });
            }

        }
    } else {
        return res.send({
            "error": true,
            "message": "Existing Password is wrong"
        });
    }
});


router.post('/superAdmin/profile/resetPassword', [verifyToken, verifySalesPersonCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id
    }

    let existingSuperAdminInfo = await superAdmin_model.getProfileById(updateRequestData.id);

    if (isEmpty(existingSuperAdminInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    const existinUserInfo = await common_model.getUserByProfileIdAndUserType(existingSuperAdminInfo[0].id, 2);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    existinUserInfo[0].password = bcrypt.hashSync("123456", 10); // hasing Password


    let result = await common_model.passwordChangeForUser(existinUserInfo[0].id, existinUserInfo[0].password);

    if (!isEmpty(result) && result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Password Reset Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Password Reset Successfully Done"
        });
    }

});


router.get('/superAdmin/Delete_list', [verifyToken, verifySalesPersonCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let result = await superAdmin_model.getSuperAdminDeactiveList();

    return res.send({
        "error": false,
        "message": "Super Admin Deactive List.",
        "data": result
    });
});


/////SUB ADMIN

router.post('/subAdmin/registration', [verifyToken, verifySalesPersonCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let userData = {
        "password": req.body.password,
        "phone_number": req.body.phone,
        "department_id": 1,
        "user_type": 3,
        "profile_id": 0
    }
    let extraUserInfo = {
        "name": req.body.name,
        "email": req.body.email
    };

    let errorMessage = "";
    let isError = 0;

    if (extraUserInfo.name == undefined || extraUserInfo.name.length < 2) {
        isError = 1;
        errorMessage = "Need valid name and length getter then 2. ";
    }

    if (userData.password == undefined || userData.password.length < 6) {
        isError = 1;
        errorMessage += "Need valid password and length getter then 6. ";
    }

    if (userData.phone_number == undefined || userData.phone_number.length != 11) {
        isError = 1;
        errorMessage += "Phone number invalid.";
    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }


    let getUserData = await common_model.getUserByPhone(userData.phone_number);
    if (!isEmpty(getUserData)) {
        return res.send({
            "error": true,
            "message": "Phone number Already Use."
        });
    } else {

        let subAdminData = {
            "name": extraUserInfo.name,
            "phone_number": userData.phone_number,
            "used_phone_no": userData.phone_number,
            "email": extraUserInfo.email,
            "image": "subAdmin.jpg",
            "active_status": 0
        }

        let result = await subAdmin_model.addNewSubAdmin(subAdminData);

        if (result.affectedRows == undefined || result.affectedRows < 1) {
            return res.send({
                "error": true,
                "message": "Some thing Wrong in system database."
            });
        }

        userData.profile_id = result.insertId;
        userData.password = bcrypt.hashSync(userData.password, 10); // hasing Password
        result = await common_model.addNewUser(userData);

        if (result.affectedRows == undefined || result.affectedRows < 1) {
            return res.send({
                "error": true,
                "message": "Some thing Wrong in system database."
            });
        }

        return res.send({
            "error": false,
            "message": "Sub admin successfully added.",
            "data": {
                "phone": userData.phone_number,
                "name": subAdminData.name
            }
        });
    }
});

router.get('/subAdmin/list', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let result = await subAdmin_model.getSubAdminList();

    return res.send({
        "error": false,
        "message": "Sales List.",
        "data": result
    });
});

router.post('/subAdmin/delete', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let id = req.body.id;

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "error": true,
            "message": "invalid id."
        });
    }

    let userData = await common_model.getUserByProfileIdAndUserType(id, 3);

    if (!isEmpty(userData)) {

        if (userData[0].phone_number === "0100") {
            return res.send({
                "error": true,
                "message": "Sub Admin Profile already Deleted."
            });
        }

        let subAdminProfile = await subAdmin_model.getProfileById(id);

        if (isEmpty(subAdminProfile)) {
            return res.send({
                "error": false,
                "message": "Unknown Sub Admin."
            });
        }

        let result = await subAdmin_model.deactiveAccount(id);
        if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
            return res.send({
                "error": true,
                "message": "Account Delete Fail try again."
            });
        }

        result = await subAdmin_model.updateProfileById(id, { "phone_number": "0100" });
        if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
            return res.send({
                "error": true,
                "message": "Account Delete Fail try again."
            });
        }

        result = await common_model.updateUserById(userData[0].id, { "phone_number": "0100" });

        if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
            return res.send({
                "error": true,
                "message": "Account Delete Fail try again."
            });
        }

        return res.send({
            "error": false,
            "message": "Sub Admin (Phone No: " + userData[0].phone_number + ", Name: " + subAdminProfile[0].name + ") Delete Successfully Done."
        });

    } else {
        return res.send({
            "error": false,
            "message": "Some thing wrong User Not found.",
            "data": []
        });
    }
});


router.post('/subAdmin/profile/update', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    const updateRequestData = {
        "id": req.body.id,
        "phone_number": req.body.phone,
        "name": req.body.name,
        "email": req.body.email
    }

    const existinUserInfo = await subAdmin_model.getProfileById(updateRequestData.id);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    let updateData = {};

    let errorMessage = "";
    let isError = 0; // 0 = No
    let willWeUpdate = 1; // 0 = yes;

    if (existinUserInfo[0].name !== updateRequestData.name) {

        if (updateRequestData.name == undefined || updateRequestData.name.length < 2) {
            isError = 1;
            errorMessage = "Need valid name and length getter then 2. ";
        } else {
            willWeUpdate = 0;
            updateData.name = updateRequestData.name;
        }
    }

    //use in future
    // if (existinUserInfo[0].phone_number !== updateRequestData.phone_number) {

    //     if (updateRequestData.phone_number == undefined || updateRequestData.phone_number.length != 11) {
    //         isError = 1;
    //         errorMessage += "Phone number invalid. ";
    //     } else {

    //         let userINPhoneNumber = await common_model.getUserByPhone(updateRequestData.phone_number);

    //         if (!isEmpty(userINPhoneNumber)){
    //             isError = 1;
    //             errorMessage += "Phone number already use.";
    //         } else {
    //             willWeUpdate = 0;
    //             updateData.phone_number = updateRequestData.phone_number;
    //         }

    //     }
    // }

    if (existinUserInfo[0].email !== updateRequestData.email) {

        if (isEmpty(updateRequestData.email)) {
            willWeUpdate = 0;
            updateData.email = updateRequestData.email;
        } else {
            const re = /\S+@\S+\.\S+/;
            if (!re.test(updateRequestData.email)) {
                isError = 1;
                errorMessage += " Email is invalid.";
            } else {
                willWeUpdate = 0;
                updateData.email = updateRequestData.email;
            }
        }

    }

    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    let result = {};
    if (willWeUpdate == 0) {
        if (isEmpty(updateData)) {
            return res.send({
                "error": false,
                "message": "You are not changing data."
            });
        } else {
            result = await subAdmin_model.updateProfileById(updateRequestData.id, updateData);
        }
    }

    if (isEmpty(result) || !result.hasOwnProperty("affectedRows") || (result.affectedRows == 0)) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Update Successfully Done"
        });
    }

});

router.get('/subAdmin/profile/view/:id', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    const id = req.params.id;

    const userInfo = await subAdmin_model.getProfileById(id);

    if (isEmpty(userInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    } else {
        delete userInfo[0].active_status;
        return res.send({
            "error": false,
            "message": "User Found.",
            "data": userInfo[0]
        });
    }
});

router.post('/subAdmin/profile/passwordChange', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id,
        "old_password": req.body.old_password,
        "new_password": req.body.new_password,
    }

    let existingSubAdminInfo = await subAdmin_model.getProfileById(updateRequestData.id);




    if (isEmpty(existingSubAdminInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    const existinUserInfo = await common_model.getUserByProfileIdAndUserType(existingSubAdminInfo[0].id, 3);


    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }


    if (bcrypt.compareSync(updateRequestData.old_password, existinUserInfo[0].password)) {
        if (updateRequestData.new_password == undefined || updateRequestData.new_password.length < 6) {
            return res.send({
                "error": true,
                "message": "Give Valid Password and Length should be greater than 6."
            });
        } else {

            updateRequestData.new_password = bcrypt.hashSync(updateRequestData.new_password, 10); // hasing Password
            let result = await common_model.passwordChangeForUser(existinUserInfo[0].id, updateRequestData.new_password);

            if (!isEmpty(result) && result.affectedRows == 0) {
                return res.send({
                    "error": true,
                    "message": "Update Fail try again"
                });
            } else {
                return res.send({
                    "error": false,
                    "message": "Update Successfully Done"
                });
            }

        }
    } else {
        return res.send({
            "error": true,
            "message": "Existing Password is wrong"
        });
    }

});

router.post('/subAdmin/profile/resetPassword', [verifyToken, verifySalesPersonCantAccess], async (req, res) => {

    let updateRequestData = {
        "id": req.body.id
    }

    let existingSubAdminInfo = await subAdmin_model.getProfileById(updateRequestData.id);

    if (isEmpty(existingSubAdminInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    const existinUserInfo = await common_model.getUserByProfileIdAndUserType(existingSubAdminInfo[0].id, 3);

    if (isEmpty(existinUserInfo)) {
        return res.send({
            "error": true,
            "message": "User Not Exist."
        });
    }

    existinUserInfo[0].password = bcrypt.hashSync("123456", 10); // hasing Password
    let result = await common_model.passwordChangeForUser(existinUserInfo[0].id, existinUserInfo[0].password);

    if (!isEmpty(result) && result.affectedRows == 0) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    } else {
        return res.send({
            "error": false,
            "message": "Password Reset Successfully Done"
        });
    }

});

router.get('/subAdmin/Delete_list', [verifyToken, verifySalesPersonCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let result = await subAdmin_model.getSubAdminDeactiveList();

    return res.send({
        "error": false,
        "message": "Sub Admin Deactive List.",
        "data": result
    });
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});



module.exports = router;