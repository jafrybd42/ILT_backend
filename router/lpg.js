const express = require("express");
const router = express.Router();
const authentication = require('./authentication');
const admin = require('./admin');
const salesPerson = require('./salesPerson');
const question = require('./question');
const clientType = require('./clientType');
const discussionType = require('./discussionType');
const vehicleType = require('./vehicleType');
const callTime = require('./callTime');
const prospectType = require('./prospectType');
const lead = require('./leads');
const companies = require('./companies.js');
const company_sister_concerns = require('./company_sister_concerns.js');
const company_sister_concern_contacts = require('./company_sister_concerns_contacts.js');
const departments = require('./departments');
const client = require('./client');
const appointment = require('./appointment');
const task = require('./task');
const dashboard = require('./dashboard');
const reminders = require('./reminders');


router.use('/authentication', authentication);
router.use('/admin', admin);
router.use('/salesPerson', salesPerson);
router.use('/question', question);
router.use('/client_type', clientType);
router.use('/discussion_type', discussionType);
router.use('/vehicle_type', vehicleType);
router.use('/call_time', callTime);
router.use('/prospect_type', prospectType);
router.use('/lead', lead);
router.use('/company', companies);
router.use('/company_sister_concerns', company_sister_concerns);
router.use('/company_sister_concern_contacts', company_sister_concern_contacts);
router.use('/department', departments);
router.use('/client', client);
router.use('/appointment', appointment);
router.use('/task', task);
router.use('/dashboard', dashboard);
router.use('/reminders', reminders);


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});



router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});



module.exports = router;