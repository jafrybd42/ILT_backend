const express = require("express");
const isEmpty = require("is-empty");
const moment = require("moment");
const router = express.Router();
const verifyToken = require('../jwt/verify/verifyLpgToken');
const reminders_model = require('../model/reminders');
const appointment_model = require('../model/appointment');

const task_model = require('../model/task');
const access_client_model = require('../model/access_client');
const task_discussion_types_model = require('../model/task_discussion_types');
const leads_model = require('../model/leads');
const prospect_type_model = require('../model/prospectType');
const company_sister_concerns_model = require('../model/company_sister_concerns');
const access_corporate_client_model = require('../model/access_corporate_clients');
const client_model = require('../model/client');

const verifySalesPersonCantAccess = require('../jwt/verify/verifyLpgSalesPersonCantAccess');
const verifySuperAdminCantAccess = require('../jwt/verify/verifyLpgSuperAdminCantAccess');
const verifySubAdminCantAccess = require('../jwt/verify/verifyLpgSubAdminCantAccess');




router.get(['/list', '/dashboardReminders'], verifyToken, async (req, res) => {

    let userType = req.decoded.user_type;
    let sales_person_id = userType == 4 ? req.decoded.id : 0; // if request user is a sales person, she/he can only access his/her task data
    const d = new Date();
    const today = "'" + d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + "'";

    let resultList = await reminders_model.getListByDate(userType, sales_person_id, today, today);

    for (let i = 0; i < resultList.length; i++) {
        resultList[i].appointmentDetails = await appointment_model.getAppointmentByID(resultList[i].appointment_id);

        delete resultList[i].updated_at;
        delete resultList[i].created_at;

        if (!isEmpty(resultList[i].appointmentDetails)) {
            if (resultList[i].appointmentDetails[0].client_type_id == 0) {
                resultList[i].appointmentDetails[0].client_info = await company_sister_concerns_model.getSisterConcernAndCompanyByID(resultList[i].appointmentDetails[0].client_id);
            } else
                resultList[i].appointmentDetails[0].client_info = await client_model.getClientByID(resultList[i].appointmentDetails[0].client_id);

            if (!isEmpty(resultList[i].appointmentDetails[0].client_info)) { // Delete some data
                if (resultList[i].appointmentDetails[0].client_info[0].hasOwnProperty("user_id")) delete resultList[i].appointmentDetails[0].client_info[0].user_id;
                if (resultList[i].appointmentDetails[0].client_info[0].hasOwnProperty("created_at")) delete resultList[i].appointmentDetails[0].client_info[0].created_at;
                if (resultList[i].appointmentDetails[0].client_info[0].hasOwnProperty("active_status")) delete resultList[i].appointmentDetails[0].client_info[0].active_status;
            }

            delete resultList[i].appointmentDetails[0].reason_for_delete;
        }
    }

    return res.send({
        "error": false,
        "message": userType == 4 ? "Reminders List for SalesPerson." : "Reminders for admin",
        "total_data": resultList.length,
        "date": today,
        "data": resultList
    });
});


router.post('/create_task', [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let reminderId = req.body.reminder_id;
    let lead_id = req.body.lead_id;
    let call_time = req.body.call_time;
    let prospect_type_id = req.body.prospect_type_id;
    let call_date = req.body.call_date;
    let salesPersonId = req.decoded.id === undefined ? 0 : req.decoded.id;

    let errorMessage = "";
    let isError = 0;

    let reminderList = await reminders_model.getReminderById(reminderId);

    if (isEmpty(reminderList)) {
        return res.send({
            "error": false,
            "message": "Unknown Reminders"
        });
    }

    if (reminderList[0].sales_person_id !== salesPersonId) {
        return res.send({
            "error": true,
            "message": "Unauthorize access request. It's not your reminders."
        });
    }

    if (reminderList[0].is_created_task !== 0) {
        return res.send({
            "error": true,
            "message": "Task Already Created. You can create one Task for each Reminders."
        });
    }

    let m = moment(call_date, 'YYYY-MM-DD', true);
    if (!m.isValid()) {
        return res.send({
            "error": true,
            "message": "call_time in Invalid"
        });
    }

    // Check given Data start
    var d = new Date();
    var today = new Date(d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate());

    if (today.getTime() > new Date(call_date).getTime()) {
        isError = 1;
        errorMessage += "Please add a day which is greater then yesterday.";
    }

    if (call_time == undefined || isEmpty(call_time)) {
        isError = 1;
        errorMessage += "Time Should be Added. ";

    }




    let check_lead = await leads_model.getLeadByID(lead_id);
    if (lead_id == undefined || isEmpty(check_lead) || check_lead[0].active_status === 1) {
        isError = 1;
        errorMessage += "Leads not found. ";
    }

    let check_prospect_type = await prospect_type_model.getProspectTypeByID(prospect_type_id);

    if (prospect_type_id == undefined || prospect_type_id === 0) {
        prospect_type_id = 0;
    } else {
        if (isEmpty(check_prospect_type) || check_prospect_type[0].active_status === 1) {
            isError = 1;
            errorMessage += "Prospect Type not found. ";
        }
    }


    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }


    // Update Reminder
    let result = await reminders_model.changeIs_Creates_taskById(reminderId);

    if (isEmpty(result)) {
        return res.send({
            "error": true,
            "message": "Update Fail try again"
        });
    }

    let appintmentInfo = await appointment_model.getAppointmentByID(reminderList[0].appointment_id);

    if (isEmpty(appintmentInfo)) {
        return res.send({
            "error": true,
            "message": "Appointment ID Not Exist."
        });
    }

    let newTaskData = {
        "client_id": appintmentInfo[0].client_id,
        "client_type_id": appintmentInfo[0].client_type_id,
        "prospect_type_id": prospect_type_id,
        "sales_person_id": salesPersonId,
        "created_by": salesPersonId,
        "call_date": call_date,
        "lead_id": lead_id,
        "call_time": call_time,
        "active_status": 0
    }

    errorMessage = "Reminder update. But Task not create as .. ";

    if (newTaskData.client_type_id == 0) {

        // check sister_concern is exist
        let sister_concern_details = await company_sister_concerns_model.getSisterConcernByID(newTaskData.client_id);

        if (newTaskData.client_id == undefined || isEmpty(sister_concern_details)) {
            isError = 1;
            errorMessage += "Client not found. ";
        } else {
            if (sister_concern_details[0].active_status == 1) {
                return res.send({
                    "error": true,
                    "message": "As client Deactive, You Never create Task for this reminder"
                });
            }

            // check sales person can access this sister concern

            let sister_concern_access_details = await access_corporate_client_model.getAccessCorporateClientDataBycompany_sister_concern_idAndsales_person_id(newTaskData.client_id, newTaskData.sales_person_id);

            if (isEmpty(sister_concern_access_details)) {
                return res.send({
                    "error": true,
                    "message": "This is not you company Sister concer so that You Never create Task for this reminder."
                });
            }
        }

    } else {
        if (newTaskData.client_id == undefined || isEmpty(await client_model.getClientByID(newTaskData.client_id))) {
            isError = 1;
            errorMessage += "Client not found. ";
        }

        // check sales person can access this client

        let client_access_details = await access_client_model.getAccessClientDataByclient_idAndsales_person_id(newTaskData.client_id, newTaskData.sales_person_id);

        if (isEmpty(client_access_details)) {
            return res.send({
                "error": true,
                "message": "This is not your Client So You Never create Task for this reminder."
            });
        }
    }


    if (isError == 1) {
        return res.send({
            "error": true,
            "message": errorMessage
        });
    }

    // Create Task
    result = await task_model.addNewTask(newTaskData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database."
        });
    }

    let taskId = result.insertId;
    let newTaskDiscussion_Type = {
        "task_id": taskId,
        "discussion_type_id": appintmentInfo[0].discussion_type_id
    }

    // Create Disscussion Type
    result = await task_discussion_types_model.addNew(newTaskDiscussion_Type);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "error": true,
            "message": "Some thing Wrong in system database. [Discussion]. "
        });
    }

    return res.send({
        "error": false,
        "message": "New Task Create."
    });
});

router.post(['/search'], [verifyToken, verifySuperAdminCantAccess, verifySubAdminCantAccess], async (req, res) => {

    let search_date = {};
    let userType = req.decoded.user_type;
    let sales_person_id = userType == 4 ? req.decoded.id : 0; // if request user is a sales person, she/he can only access his/her task data

    let d = new Date();
    let reminder_start_date = "'" + d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + "'";
    let reminder_end_date = "'" + d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + "'";

    // Generate Search by date
    if (req.body.from_date !== undefined && req.body.from_date != "") {
        if (req.body.to_date !== undefined && req.body.to_date != "") {

            let m = moment(req.body.from_date, 'YYYY-MM-DD', true);
            if (m.isValid()) {
                reminder_start_date = "'" + req.body.from_date + "'";
            } else {
                return res.send({
                    "error": true,
                    "message": "From Date in Invalid"
                });
            }

            m = moment(req.body.to_date, 'YYYY-MM-DD', true);
            if (m.isValid()) {
                reminder_end_date = "'" + req.body.to_date + "'";
            } else {
                return res.send({
                    "error": true,
                    "message": "To Date in Invalid"
                });
            }

        } else {
            let m = moment(req.body.from_date, 'YYYY-MM-DD', true);
            if (m.isValid()) {
                reminder_start_date = "'" + req.body.from_date + "'";
                reminder_end_date = "'" + req.body.from_date + "'";
            } else {
                return res.send({
                    "error": true,
                    "message": "From Date in Invalid"
                });
            }
        }
    }


    // let resultList = await reminders_model.getListBySearching(userType, sales_person_id, search_date);
    let resultList = await reminders_model.getListByDate(userType, sales_person_id, reminder_start_date, reminder_end_date);

    for (let i = 0; i < resultList.length; i++) {
        resultList[i].appointmentDetails = await appointment_model.getAppointmentByID(resultList[i].appointment_id);

        delete resultList[i].updated_at;
        delete resultList[i].created_at;

        if (!isEmpty(resultList[i].appointmentDetails)) {
            if (resultList[i].appointmentDetails[0].client_type_id == 0) {
                resultList[i].appointmentDetails[0].client_info = await company_sister_concerns_model.getSisterConcernAndCompanyByID(resultList[i].appointmentDetails[0].client_id);
            } else
                resultList[i].appointmentDetails[0].client_info = await client_model.getClientByID(resultList[i].appointmentDetails[0].client_id);

            if (!isEmpty(resultList[i].appointmentDetails[0].client_info)) { // Delete some data
                if (resultList[i].appointmentDetails[0].client_info[0].hasOwnProperty("user_id")) delete resultList[i].appointmentDetails[0].client_info[0].user_id;
                if (resultList[i].appointmentDetails[0].client_info[0].hasOwnProperty("created_at")) delete resultList[i].appointmentDetails[0].client_info[0].created_at;
                if (resultList[i].appointmentDetails[0].client_info[0].hasOwnProperty("active_status")) delete resultList[i].appointmentDetails[0].client_info[0].active_status;
            }

            delete resultList[i].appointmentDetails[0].reason_for_delete;
        }

    }

    return res.send({
        "error": false,
        "message": userType == 4 ? "Reminders List for SalesPerson." : "Reminders for admin",
        "total_data": resultList.length,
        "date_range": reminder_start_date + " to " + reminder_end_date,
        "data": resultList
    });
});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

module.exports = router;