const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const question_model = require('../model/passwordRecoveryQuestion');
const verifyToken = require('../jwt/verify/verifyLpgToken');



router.get('/list', async (req, res) => {

    let result = await question_model.getQuestionList();

    return res.send({
        "error": false,
        "message": "Message List.",
        "data": result
    });
});



router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "error": true
    })
});

module.exports = router;